/* eslint-disable import/prefer-default-export */
/* eslint-disable node/no-unsupported-features/es-syntax */
import styled from 'styled-components';
import { pixelToRem } from '../../styles/globals';

export const BookDiv = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 100px);
  margin: ${pixelToRem(8)};
  grid-gap: ${pixelToRem(20)};
  border-radius: ${pixelToRem(4)};
  background-color: #fff;
  cursor: pointer;

  width: ${pixelToRem(288)};
  height: ${pixelToRem(160)};

  box-shadow: 0px ${pixelToRem(6)} ${pixelToRem(24)} rgba(84, 16, 95, 0.13);

  .book-cover {
    display: flex;
    justify-content: center;
    width: ${pixelToRem(81)};
    height: ${pixelToRem(122)};
    margin: ${pixelToRem(19)} ${pixelToRem(175)} ${pixelToRem(16)}
      ${pixelToRem(16)};
    filter: drop-shadow(0px 6px 9px rgba(0, 0, 0, 0.15));

    img {
      width: ${pixelToRem(81)};
      height: ${pixelToRem(122)};
    }
  }

  @media (min-width: 998px) {
    margin: ${pixelToRem(4)};
    width: ${pixelToRem(272)};
    height: ${pixelToRem(175)};
  }
`;

export const InfoDiv = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: ${pixelToRem(16)};
  padding-bottom: ${pixelToRem(16)};

  .title-div {
    height: ${pixelToRem(20)};
    margin-bottom: ${pixelToRem(10)};
  }

  .author-div {
    height: ${pixelToRem(20)};
    margin-top: ${pixelToRem(8)};
    margin-bottom: ${pixelToRem(25)};
  }
`;

export const TitleDiv = styled.h1`
  width: ${pixelToRem(140)};
  height: ${pixelToRem(15)};

  font-family: Heebo;
  font-style: normal;
  font-weight: 500;
  font-size: ${pixelToRem(12)};
  line-height: ${pixelToRem(16)};
  color: #333;
`;

export const AuthorName = styled.h3`
  width: ${pixelToRem(130)};

  font-family: Heebo;
  font-style: normal;
  font-weight: normal;
  font-size: ${pixelToRem(11)};
  line-height: ${pixelToRem(12)};

  color: #ab2680;
`;

export const InfoBook = styled.div`
  .info-div {
    width: ${pixelToRem(130)};
    height: ${pixelToRem(60)};
    margin-top: ${pixelToRem(4)};
    margin-bottom: ${pixelToRem(16)};
  }

  h3 {
    width: ${pixelToRem(130)};
    font-family: Heebo;
    font-style: normal;
    font-weight: normal;
    font-size: ${pixelToRem(11)};
    line-height: ${pixelToRem(15)};

    color: #999;
  }
`;
