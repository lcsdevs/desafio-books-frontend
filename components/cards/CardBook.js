/* eslint-disable no-undef */
/* eslint-disable node/no-unsupported-features/es-syntax */
import React, { useState, useEffect } from 'react';
import lodash from 'lodash';
import {
  BookDiv,
  InfoDiv,
  TitleDiv,
  AuthorName,
  InfoBook,
} from './CardBookStyle';
import nullCover from '../../assets/books/nullcover.svg';
import i18n from '../../utils/i18n/pt_BR.json';
import { CloseButton } from '../popup/PopupStyle';
import Popup from '../popup/Popup';

export default function Book({ book }) {
  const [popup, setPopup] = useState(false);

  const closeModal = event => {
    setPopup(false);
  };

  const showModal = () => {
    setPopup(true);
  };

  const bookCover = _.isNull(book.imageUrl) ? nullCover : book.imageUrl;

  return (
    <BookDiv>
      {popup && (
        <>
          <CloseButton onClick={closeModal} />
          <Popup bookId={book.id} />
        </>
      )}
      <div className="book-cover" onClick={showModal}>
        <img src={bookCover} alt="" />
      </div>
      <InfoDiv onClick={showModal}>
        <div className="title-div">
          <TitleDiv>{book.title}</TitleDiv>
        </div>
        <div className="author-div">
          {book?.authors?.map(author => (
            <AuthorName>{author}</AuthorName>
          ))}
        </div>
        <InfoBook>
          <div className="info-div">
            <h3>
              {book.pageCount}
              {i18n.BOOK.PAGES}
            </h3>
            <h3>{book.publisher}</h3>
            <h3>
              {i18n.BOOK.PUBLISHED}
              {book.published}
            </h3>
          </div>
        </InfoBook>
      </InfoDiv>
    </BookDiv>
  );
}
