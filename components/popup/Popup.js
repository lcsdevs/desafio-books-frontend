/* eslint-disable node/no-unsupported-features/es-syntax */
import React, { useState, useEffect } from 'react';
import lodash from 'lodash';
import {
  PopupDiv,
  PopupContent,
  InfoDiv,
  TitleDiv,
  AuthorName,
  InfoBook,
  DescDiv,
} from './PopupStyle';
import { getBookbyId } from '../../services/bookService';
import nullCover from '../../assets/books/nullcover.svg';
import quote from '../../assets/popup/quote.svg';
import i18n from '../../utils/i18n/pt_BR.json';

export default function Popup({ bookId }) {
  const [book, setBook] = useState([]);

  async function getBook() {
    const token = localStorage.getItem('auth');
    const { data } = await getBookbyId({ bookId, token });
    setBook(data);
  }

  useEffect(() => {
    getBook();
  }, []);
  console.log(book);
  const bookCover = _.isNull(book.imageUrl) ? nullCover : book.imageUrl;

  return (
    <PopupDiv>
      <PopupContent>
        <div className="book-cover">
          <img src={bookCover} alt="" />
        </div>
        <InfoDiv>
          <div className="title-div">
            <TitleDiv>{book.title}</TitleDiv>
          </div>
          <div className="author-div">
            {book?.authors?.map(author => (
              <AuthorName>{author} ,</AuthorName>
            ))}
          </div>
          <InfoBook>
            <div className="info-div">
              <h1>{i18n.BOOK.INFO}</h1>
              <h3>{i18n.BOOK.PAGES}</h3>
              <h3>{i18n.BOOK.EDIT}</h3>
              <h3>{i18n.BOOK.PUBLISHED_1}</h3>
              <h3>{i18n.BOOK.LANGUAGE}</h3>
              <h3>{i18n.BOOK.TITLE}</h3>
              <h3>{i18n.BOOK.ISB10}</h3>
              <h3>{i18n.BOOK.ISB13}</h3>
            </div>
            <div className="info-data">
              <h1 />
              <h3>
                {book.pageCount}
                {i18n.BOOK.PAGES}
              </h3>
              <h3>{book.publisher}</h3>
              <h3>{book.published}</h3>
              <h3>{book.language}</h3>
              <h3>{book.title}</h3>
              <h3>{book.isbn10}</h3>
              <h3>{book.isbn13}</h3>
            </div>
          </InfoBook>
          <DescDiv>
            <h1>{i18n.BOOK.DESC}</h1>
            <img src={quote} alt="" />
            <h3>{book.description}</h3>
          </DescDiv>
        </InfoDiv>
      </PopupContent>
    </PopupDiv>
  );
}
