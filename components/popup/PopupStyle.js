/* eslint-disable import/prefer-default-export */
/* eslint-disable node/no-unsupported-features/es-syntax */
import styled from 'styled-components';
import { pixelToRem } from '../../styles/globals';
import close from '../../assets/popup/close.svg';

export const PopupDiv = styled.div`
  position: absolute;
  width: 100vw;
  height: 100vh;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 1;
`;

export const PopupContent = styled.div`
  position: absolute;
  left: 25%;
  right: 25%;
  top: 25%;
  bottom: 25%;
  margin: auto;
  border-radius: ${pixelToRem(4)};
  background: #ffffff;
  box-shadow: 0px 16px 80px rgba(0, 0, 0, 0.32);
  border-radius: 4px;
  z-index: 500;

  width: ${pixelToRem(288)};
  height: ${pixelToRem(970)};

  .book-cover {
    display: flex;
    justify-content: center;
    width: ${pixelToRem(240)};
    height: ${pixelToRem(351)};
    margin: ${pixelToRem(16)};
    filter: drop-shadow(0px 6px 9px rgba(0, 0, 0, 0.15));

    img {
      width: ${pixelToRem(240)};
      height: ${pixelToRem(351)};
    }
  }

  @media (min-width: 998px) {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    width: ${pixelToRem(769)};
    height: ${pixelToRem(608)};
    .book-cover {
      display: flex;
      justify-content: center;
      width: ${pixelToRem(349)};
      height: ${pixelToRem(512)};
      margin: ${pixelToRem(48)} ${pixelToRem(48)} ${pixelToRem(48)}
        ${pixelToRem(48)};

      img {
        width: ${pixelToRem(349)};
        height: ${pixelToRem(512)};
      }
    }
  }
`;

export const CloseButton = styled.button`
  position: fixed;
  top: 10px;
  right: 20px;
  width: ${pixelToRem(32)};
  height: ${pixelToRem(32)};
  cursor: pointer;
  outline: none;
  background: url(${close}) no-repeat;
  z-index: 500;
`;

export const InfoDiv = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: ${pixelToRem(48)} ${pixelToRem(16)} ${pixelToRem(51)}
    ${pixelToRem(16)};

  .title-div {
    height: ${pixelToRem(80)};
    margin-bottom: ${pixelToRem(2)};
  }

  .author-div {
    display: grid;
    grid-template-columns: repeat(3, 100px);
    height: ${pixelToRem(10)};
    margin-top: ${pixelToRem(2)};
    margin-bottom: ${pixelToRem(25)};
  }

  @media (min-width: 998px) {
    .title-div {
      height: ${pixelToRem(80)};
      margin-bottom: ${pixelToRem(2)};
    }

    .author-div {
      display: grid;
      grid-template-columns: repeat(3, 100px);
      height: ${pixelToRem(10)};
      margin-top: ${pixelToRem(2)};
      margin-bottom: ${pixelToRem(25)};
    }
  }
`;

export const TitleDiv = styled.h1`
  width: ${pixelToRem(276)};
  height: ${pixelToRem(80)};

  font-family: Heebo;
  font-style: normal;
  font-weight: 500;
  font-size: ${pixelToRem(28)};
  line-height: ${pixelToRem(40)};
  color: #333;
`;

export const AuthorName = styled.h3`
  width: ${pixelToRem(100)};

  font-family: Heebo;
  font-style: normal;
  font-weight: normal;
  font-size: ${pixelToRem(12)};
  line-height: ${pixelToRem(20)};

  color: #ab2680;
`;

export const InfoBook = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  margin: ${pixelToRem(24)} 0;

  .info-div {
    width: ${pixelToRem(84)};
    height: ${pixelToRem(175)};
    margin-bottom: ${pixelToRem(10)};

    h1 {
      width: ${pixelToRem(130)};
      font-family: Heebo;
      font-style: normal;
      font-weight: 500;
      font-size: ${pixelToRem(18)};
      line-height: ${pixelToRem(20)};
      margin-bottom: ${pixelToRem(10)};
    }

    h3 {
      width: ${pixelToRem(130)};
      font-family: Heebo;
      font-style: normal;
      font-weight: 500;
      font-size: ${pixelToRem(12)};
      line-height: ${pixelToRem(20)};
    }
  }

  .info-data {
    width: ${pixelToRem(97)};
    height: ${pixelToRem(140)};
    margin: ${pixelToRem(28)} 0;

    h3 {
      width: ${pixelToRem(130)};
      font-family: Heebo;
      font-style: normal;
      font-weight: normal;
      font-size: ${pixelToRem(12)};
      line-height: ${pixelToRem(20)};
      color: #999999;
    }
  }
`;

export const DescDiv = styled.div`
  display: flex;
  flex-direction: column;

  img {
    margin-top: ${pixelToRem(12)};
    width: ${pixelToRem(17)};
    height: ${pixelToRem(14)};
    opacity: 0.5;
  }

  h1 {
    font-size: ${pixelToRem(12)};
    line-height: ${pixelToRem(14)};
  }

  h3 {
      width: ${pixelToRem(260)};
      height: ${pixelToRem(170)};

      font-family: Heebo;
      font-style: normal;
      font-weight: 500;
      font-size: ${pixelToRem(9)};
      line-height: ${pixelToRem(12)};
      color: #999999;
    

  @media (min-width: 998px) {
    h3 {
      width: ${pixelToRem(276)};
      height: ${pixelToRem(170)};

      font-family: Heebo;
      font-style: normal;
      font-weight: 500;
      font-size: ${pixelToRem(10)};
      line-height: ${pixelToRem(14)};
      color: #999999;
    }
  }
`;
