/* eslint-disable node/no-unsupported-features/es-syntax */
import React, { useState } from 'react';
import Link from 'next/link';
import {
  BodyStyled,
  InputDiv,
  InputField,
  LabelText,
  ButtonLogin,
  ErrorMessage,
} from './LoginStyle';
import header from '../../assets/header.svg';
import i18n from '../../utils/i18n/pt_BR.json';
import { getUserAuthetication } from '../../services/loginService';

export default function Login() {
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [error, setError] = useState(false);

  async function getUserToken() {
    const { data, headers, status } = await getUserAuthetication({
      email,
      password,
    });

    if (status === 200) {
      localStorage.setItem('auth', headers?.authorization);
      localStorage.setItem('name', data?.name);
      window.location.href = '/books';
      setError(false);
    } else {
      setError(true);
    }
  }

  return (
    <BodyStyled>
      <InputDiv>
        <div className="header">
          <img src={header} alt="ioasy api logo" />
        </div>
        <div className="input">
          <InputField
            value={email}
            type="e-mail"
            onChange={event => setEmail(event.target.value)}
          />
          <LabelText>{i18n.LOGIN.EMAIL}</LabelText>
        </div>
        <div className="input">
          <InputField
            type="password"
            onChange={event => setPassword(event.target.value)}
          />
          <LabelText type="password">{i18n.LOGIN.PASSWORD}</LabelText>
          <ButtonLogin onClick={getUserToken}>{i18n.LOGIN.BUTTON}</ButtonLogin>
        </div>
        {error && (
          <div>
            <ErrorMessage>
              <div className="triangle" />
              <h1>{i18n.LOGIN.ERROR}</h1>
            </ErrorMessage>
          </div>
        )}
      </InputDiv>
    </BodyStyled>
  );
}
