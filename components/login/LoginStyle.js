/* eslint-disable node/no-unsupported-features/es-syntax */
import styled from 'styled-components';
import { pixelToRem } from '../../styles/globals';
import background from '../../assets/background.svg';
import triagle from '../../assets/triangle.svg';

export const BodyStyled = styled.div`
  height: 100vh;
  background: url(${background}) no-repeat;
  background-size: cover;
`;

export const InputDiv = styled.div`
  display: flex;
  flex-direction: column;
  padding: ${pixelToRem(208)} ${pixelToRem(16)};

  .header {
    width: ${pixelToRem(198)};
    height: ${pixelToRem(90)};
    padding-bottom: ${pixelToRem(48)};
  }

  .input {
    padding-bottom: ${pixelToRem(16)};
  }

  @media (min-width: 998px) {
    padding: ${pixelToRem(272)} ${pixelToRem(500)} ${pixelToRem(272)}
      ${pixelToRem(115)};
  }
`;

export const InputField = styled.input.attrs(props => ({
  type: props.type,
}))`
  background: rgba(0, 0, 0, 0.32);
  backdrop-filter: blur(2px);
  width: ${pixelToRem(288)};
  height: ${pixelToRem(60)};
  border-radius: ${pixelToRem(5)};
  padding: ${pixelToRem(28)} ${pixelToRem(100)} ${pixelToRem(8)}
    ${pixelToRem(16)};

  font-family: Heebo;
  font-style: normal;
  font-weight: normal;
  font-size: ${pixelToRem(16)};
  line-height: ${pixelToRem(24)};
  color: #fff;

  border-color: transparent;
  box-shadow: 0px;
  outline: none;
  transition: 0.15s;

  @media (min-width: 998px) {
    width: ${pixelToRem(368)};
  }
`;

export const LabelText = styled.label`
  position: absolute;
  width: ${pixelToRem(30)};
  height: ${pixelToRem(16)};
  margin-top: ${pixelToRem(10)};
  left: ${pixelToRem(29)};

  font-family: Heebo;
  font-style: normal;
  font-weight: normal;
  font-size: ${pixelToRem(12)};
  line-height: ${pixelToRem(16)};

  color: #fff;
  z-index: 1;
  opacity: 0.5;
  @media (min-width: 998px) {
    margin-top: ${pixelToRem(10)};
    left: ${pixelToRem(131)};
  }
`;

export const ButtonLogin = styled.button`
  position: absolute;
  width: ${pixelToRem(85)};
  height: ${pixelToRem(36)};
  left: ${pixelToRem(203)};
  top: ${pixelToRem(384)};

  background: #fff;
  border-radius: ${pixelToRem(44)};
  cursor: pointer;
  outline: none;

  font-family: Heebo;
  font-style: bold;
  font-weight: 400;
  font-size: ${pixelToRem(16)};
  line-height: ${pixelToRem(20)};
  color: #b22e6f;
  transition: 0.3s;

  &:hover {
    background: #b22e6f;
    color: #fff;
  }

  @media (min-width: 998px) {
    top: ${pixelToRem(448)};
    left: ${pixelToRem(386)};
  }
`;

export const ErrorMessage = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;

  width: ${pixelToRem(239)};
  height: ${pixelToRem(48)};

  background: rgba(255, 255, 255, 0.4);
  backdrop-filter: blur(2px);
  border-radius: ${pixelToRem(4)};

  .triangle {
    position: absolute;
    top: -10px;
    left: 11px;
    width: ${pixelToRem(8)};
    height: ${pixelToRem(16)};
    background: url(${triagle}) no-repeat;
    transform: rotate(225deg);
    z-index: 1;
  }

  h1 {
    margin: ${pixelToRem(16)};
    width: ${pixelToRem(207)};
    height: ${pixelToRem(16)};

    font-family: Heebo;
    font-style: normal;
    font-weight: bold;
    font-size: ${pixelToRem(16)};
    line-height: ${pixelToRem(16)};
    color: #fff;
  }
`;
