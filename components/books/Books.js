/* eslint-disable node/no-unsupported-features/es-syntax */
import React, { useState, useEffect } from 'react';
import { useMediaQuery } from 'react-responsive';
import {
  BodyStyled,
  BookWrapper,
  BoookContent,
  LogoutButton,
  PaginationFooter,
  PaginationButton,
} from './BookStyle';
import Book from '../cards/CardBook';
import logo from '../../assets/books/logo.svg';
import title from '../../assets/books/title.svg';
import i18n from '../../utils/i18n/pt_BR.json';
import { getAllBooks } from '../../services/bookService';

export default function Books() {
  const [offset, setOffset] = useState(1);
  const [totalPages, setTotalPages] = useState();
  const [books, setBooks] = useState([]);
  const [name, setName] = useState();
  const isDesktop = useMediaQuery({
    query: '(min-width:998px)',
  });
  const isMobile = useMediaQuery({
    query: '(max-width:997px)',
  });

  const handlePageClickNext = () => {
    setOffset(offset + 1);
  };

  const handlePageClickPrevious = () => {
    setOffset(offset - 1);
  };

  async function getAllBooksPerPage() {
    const token = localStorage.getItem('auth');
    const { data } = await getAllBooks({ offset, token });
    setTotalPages(Math.round(data.totalPages));
    setBooks(data);
    setName(localStorage.getItem('name'));
  }

  function Logout() {
    localStorage.clear();
    window.location.href = '/';
  }

  useEffect(() => {
    getAllBooksPerPage();
  }, [offset]);

  return (
    <BodyStyled>
      <BookWrapper>
        <header>
          <div className="img-logo">
            <img src={logo} alt="" />
            <img src={title} alt="" />
          </div>
          <div className="info-name">
            <h1>
              {i18n.BOOKS.GREETINGS}{' '}
              <span>{name?.substr(0, name.indexOf(' '))}</span>
            </h1>
            <LogoutButton onClick={Logout} />
          </div>
        </header>
        <BoookContent>
          <div className="book-cards">
            {books?.data?.map(book => (
              <Book book={book} />
            ))}
          </div>
        </BoookContent>
        <PaginationFooter>
          {isDesktop && (
            <>
              <h1>
                {i18n.BOOKS.PAGE} {offset} {i18n.BOOKS.OF} {totalPages}
              </h1>
              <div className="button-grid">
                {offset === 1 ? (
                  <PaginationButton previousOff blocked />
                ) : (
                  <PaginationButton
                    previousOn
                    onClick={handlePageClickPrevious}
                  />
                )}
                {offset === totalPages ? (
                  <PaginationButton nextOff />
                ) : (
                  <PaginationButton nextOn onClick={handlePageClickNext} />
                )}
              </div>
            </>
          )}
          {isMobile && (
            <>
              <div className="button-grid-mobile">
                {offset === 1 ? (
                  <PaginationButton previousOff blocked />
                ) : (
                  <PaginationButton
                    previousOn
                    onClick={handlePageClickPrevious}
                  />
                )}
                <h1>
                  {i18n.BOOKS.PAGE} {offset} {i18n.BOOKS.OF} {totalPages}
                </h1>
                {offset === totalPages ? (
                  <PaginationButton nextOff />
                ) : (
                  <PaginationButton nextOn onClick={handlePageClickNext} />
                )}
              </div>
            </>
          )}
        </PaginationFooter>
      </BookWrapper>
    </BodyStyled>
  );
}
