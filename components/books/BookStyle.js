/* eslint-disable node/no-unsupported-features/es-syntax */
import styled from 'styled-components';
import { pixelToRem } from '../../styles/globals';
import background from '../../assets/books/background.svg';
import logout from '../../assets/books/logout.svg';
import nexton from '../../assets/books/nexton.svg';
import nextoff from '../../assets/books/nextoff.svg';
import prevon from '../../assets/books/prevon.svg';
import prevoff from '../../assets/books/prevoff.svg';

export const BodyStyled = styled.div`
  display: flex;
  justify-content: center;
  background: url(${background}) no-repeat center;
  background-size: cover;
`;

export const BookWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: ${pixelToRem(40)} ${pixelToRem(16)} 0rem;

  header {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    justify-content: center;
    width: ${pixelToRem(288)};
    height: ${pixelToRem(40)};
  }

  .img-logo {
    display: grid;
    grid-template-columns: repeat(2, 120px);
    align-items: center;
  }

  .info-name {
    display: flex;
    flex-direction: row;
    justify-content: left;

    h1 {
      font-size: ${pixelToRem(0)};
    }

    span {
      font-weight: bold;
    }
  }

  @media (min-width: 998px) {
    flex-direction: column;
    justify-content: center;
    padding: ${pixelToRem(40)} ${pixelToRem(144)} ${pixelToRem(72)};

    header {
      display: grid;
      grid-template-columns: repeat(2, 1fr);
      width: ${pixelToRem(1130)};
      height: ${pixelToRem(60)};
    }

    .img-logo {
      display: grid;
      grid-template-columns: repeat(2, 120px);
      align-items: center;
    }

    .info-name {
      display: grid;
      grid-template-columns: repeat(2, 145px);
      grid-gap: 10px;
      padding-left: ${pixelToRem(699)};
      justify-content: left;
      align-items: center;

      h1 {
        width: ${pixelToRem(130)};
        height: ${pixelToRem(16)};
        font-family: Heebo;
        font-style: normal;
        font-weight: normal;
        font-size: ${pixelToRem(13)};
        line-height: ${pixelToRem(15)};

        text-align: right;
      }

      span {
        font-weight: bold;
      }
    }
  }
`;

export const BoookContent = styled.div`
  display: flex;
  flex-direction: row;
  align-content: center;
  margin-top: ${pixelToRem(64)};

  .book-cards {
    padding-bottom: ${pixelToRem(32)};
  }

  @media (min-width: 998px) {
    flex-direction: column;
    align-content: center;
    width: ${pixelToRem(1136)};
    height: ${pixelToRem(576)};
    margin-top: ${pixelToRem(64)};

    .book-cards {
      padding-top: ${pixelToRem(32)};
      display: grid;
      grid-template-columns: repeat(4, 1fr);
    }
  }
`;

export const LogoutButton = styled.button`
  width: ${pixelToRem(32)};
  height: ${pixelToRem(32)};
  background: url(${logout}) no-repeat center;
  cursor: pointer;
`;

export const PaginationFooter = styled.div`
  display: grid;
  align-items: center;
  margin: ${pixelToRem(16)} ${pixelToRem(30)};
  width: ${pixelToRem(165)};

  .button-grid-mobile {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    grid-gap: ${pixelToRem(50)};

    h1 {
      position: relative;
      top: ${pixelToRem(5)};
    }
  }

  h1 {
    width: ${pixelToRem(87)};
    height: ${pixelToRem(20)};

    font-family: Heebo;
    font-style: normal;
    font-weight: normal;
    font-size: ${pixelToRem(12)};
    line-height: ${pixelToRem(20)};

    color: #333;
  }

  @media (min-width: 998px) {
    grid-template-columns: repeat(2, 100px);
    margin: ${pixelToRem(16)} 0rem ${pixelToRem(30)} ${pixelToRem(960)};
    width: ${pixelToRem(175)};
    height: ${pixelToRem(48)};

    .button-grid {
      display: grid;
      grid-template-columns: repeat(2, 40px);
    }
  }
`;

export const PaginationButton = styled.button`
  width: ${pixelToRem(32)};
  height: ${pixelToRem(32)};
  background-color: transparent;
  cursor: pointer;
  outline: none;

  ${props =>
    props.previousOff && {
      background: `url(${prevoff}) no-repeat`,
    }}

  ${props =>
    props.previousOn && {
      background: `url(${prevon}) no-repeat`,
    }}

  ${props =>
    props.nextOff && {
      background: `url(${nextoff}) no-repeat`,
    }}

  ${props =>
    props.nextOn && {
      background: `url(${nexton}) no-repeat`,
    }}
`;
