/* eslint-disable consistent-return */
/* eslint-disable import/prefer-default-export */
/* eslint-disable node/no-unsupported-features/es-syntax */
import axios from 'axios';

const baseUrl = 'https://books.ioasys.com.br/api/v1/';

export const getUserAuthetication = async props => {
  try {
    const apiUrl = `${baseUrl}auth/sign-in`;
    const body = { email: props?.email, password: props?.password };
    const response = await axios
      .post(`${apiUrl}`, body)
      .then(res => res)
      .catch(error => error.response);

    if (response !== undefined) {
      return response;
    }
  } catch (error) {
    return error;
  }
};
