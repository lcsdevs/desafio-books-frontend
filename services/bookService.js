/* eslint-disable import/prefer-default-export */
/* eslint-disable node/no-unsupported-features/es-syntax */
import axios from 'axios';

const baseUrl = 'https://books.ioasys.com.br/api/v1/';

export const getAllBooks = async props => {
  try {
    const apiUrl = `${baseUrl}books?page=${props.offset}&amount=12`;
    const config = {
      headers: {
        Authorization: `Bearer ${props.token}`,
      },
    };
    const response = await axios
      .get(`${apiUrl}`, config)
      .then(res => res)
      .catch(error => error.response);

    if (response != undefined) {
      return response;
    }
  } catch (error) {
    return error;
  }
};

export const getBookbyId = async props => {
  try {
    const apiUrl = `${baseUrl}books/${props.bookId}`;
    const config = {
      headers: {
        Authorization: `Bearer ${props.token}`,
      },
    };
    const response = await axios
      .get(`${apiUrl}`, config)
      .then(res => res)
      .catch(error => error.response);

    if (response != undefined) {
      return response;
    }
  } catch (error) {
    return error;
  }
};
