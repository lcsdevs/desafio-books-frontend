/* eslint-disable no-unused-vars */
/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable node/no-unsupported-features/es-syntax */
import Login from '../components/login/Login';

export default function Home() {
  return (
    <>
      <Login />
    </>
  );
}

export async function getStaticProps(_context) {
  return {
    props: {},
    revalidate: 60,
  };
}
