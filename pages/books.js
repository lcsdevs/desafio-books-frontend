/* eslint-disable no-unused-vars */
/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable node/no-unsupported-features/es-syntax */
import Books from '../components/books/Books';

export default function Home() {
  return (
    <>
      <Books />
    </>
  );
}

export async function getStaticProps(_context) {
  return {
    props: {},
    revalidate: 60,
  };
}
