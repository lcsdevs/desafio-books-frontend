# Sobre

Estes documento README tem como objetivo fornecer as informações necessárias para realização do projeto **ioasys books**.

# 🏗 O que fazer?

- Você deve criar seu projeto e subir em um repositório e ao finalizar, enviar o link do seu repositório para a nossa equipe. Lembre-se, NÃO é necessário criar um Pull Request para isso, nós iremos avaliar e retornar por email o resultado do seu teste.

# 🚨 Requisitos

- Seu projeto deverá ser construído utilizando **ReactJS** ou **Angular**.
- Seu projeto deverá ser construído utilizando o layout disponibilizado na descrição do teste.
- A integração com a API deve ser feita respeitando todos os contratos de OAuth.
- Projetos utilizando **ReactJS** serão aceitos testes somente em **JavaScript** buscando avaliar o entendimento completo da linguagem e não de estruturas ou dependências que abstraiam determinadas definições não alheias ao ECMAScript.

# 🕵🏻‍♂️ Itens a serem avaliados

- Estrutura do Projeto
- Boas práticas da Linguagem/Framework
- Integração com API
- Bibliotecas utilizadas
- Estilização dos componentes
- Persistência de login
- Layout responsivo
- Friendly URL
- Seu projeto deverá seguir tudo o que foi exigido na seção **O que desenvolver?**

# 🎁 Extra

Esses itens não obrigatórios, porém desejados.

[ ] Testes unitários

[x] SEO

[x] Linter

[x] Code Formater

[x] Documentação de componente

# 🖥 O que desenvolver?

Você deverá construir um projeto utilizando o layout proposto

[x] Login e acesso de Usuário já registrado

[x] Para o login usamos padrões OAuth 2.0. Na resposta de sucesso do login a api retornará 3 custom headers (access-token, client, uid);

[x] Para ter acesso as demais APIs precisamos enviar o 'authorization' no header para autorizar a requisição;

[x] Listagem de Livros

[x] Detalhamento do Livro

# 🔗 Links e Informações Importantes

## Layout

- Layout e recortes disponíveis no Figma
- https://www.figma.com/file/YXuqJUzNZcR7GveJfVWCKo/Desafio-Frontend-ioasys-books

## Integração com API

- A documentação da API está disponível a partir de uma página web (https://books.ioasys.com.br/api/docs/).

- **Documentação:** https://books.ioasys.com.br/api/docs/
- **Servidor:** https://books.ioasys.com.br/api/v1
- **Usuário de Teste:** desafio@ioasys.com.br
- **Senha de Teste:** 12341234

## Bibliotecas

No projeto foram utilizados juntamente ao ReactJs, o NextJS com o intuito de prover melhorias no SEO, o Styled-Components a biblioteca de CSS-in-JSS, para estruturar o código isoladamente. ESling e Prettier para padronizar o código em todo o projeto.

## Componentes

### Login

Responsavél por prover o Login da aplicação, o mesmo contém apenas os InputField de senha e email, e o button de login, juntamente ao modal de erro. Caso o servidor retorne a requisição como 200.

### Books

Componente responsavel por exibir a página de livros, o mesmo contém o header com a logo da aplicação e o botão para deslogar a aplicação. No 'corpo' contém a lista de cards responsável por exibir todos os livros e seus detalhes. Ao clicar no detalhes e capa é exibido um popup com todos os dados dos livros.

### Cards

Componentes estilizados para exibir todos os dados dos livros resumidamente.
